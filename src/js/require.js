require.config({
  shim: {

  },
  paths: {
    almond: "../../bower_components/almond/almond",
    curl: "../../bower_components/curl/src/curl",
    "iframe-messenger": "../../bower_components/iframe-messenger/src/iframeMessenger",
    text: "../../bower_components/requirejs-plugins/lib/text",
    async: "../../bower_components/requirejs-plugins/src/async",
    depend: "../../bower_components/requirejs-plugins/src/depend",
    font: "../../bower_components/requirejs-plugins/src/font",
    goog: "../../bower_components/requirejs-plugins/src/goog",
    image: "../../bower_components/requirejs-plugins/src/image",
    json: "../../bower_components/requirejs-plugins/src/json",
    mdown: "../../bower_components/requirejs-plugins/src/mdown",
    noext: "../../bower_components/requirejs-plugins/src/noext",
    propertyParser: "../../bower_components/requirejs-plugins/src/propertyParser",
    "Markdown.Converter": "../../bower_components/requirejs-plugins/lib/Markdown.Converter",
    angular: "../../bower_components/angular/angular",
    "angular-animate": "../../bower_components/angular-animate/angular-animate",
    lodash: "../../bower_components/lodash/dist/lodash.compat",
    "angular-ui-router": "../../bower_components/angular-ui-router/release/angular-ui-router",
    leaflet: "../../bower_components/leaflet/dist/leaflet-src",
    hammerjs: "../../bower_components/hammerjs/hammer",
    "perfect-scrollbar": "../../bower_components/perfect-scrollbar/js/perfect-scrollbar",
    "perfect-scrollbar.jquery": "../../bower_components/perfect-scrollbar/js/perfect-scrollbar.jquery",
    "blob-util": "../../bower_components/blob-util/"
  },
  packages: [
    {
      name: "blob-util",
      main: "dist/blob-util.js"
    }
  ]
});
