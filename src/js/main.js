define([
    'text!templates/appTemplate.html',
    'perfect-scrollbar',
    'blob-util'
], function(
    templateHTML,
    Ps,
    Bu
) {
   'use strict';

    function init(el, context, config, mediator) {
        // DEBUG: What we get given on boot
        console.log(el, context, config, mediator);

        window.Ps = Ps;
        window.blobUtil = Bu;

        // DOM template example
        el.innerHTML = templateHTML;

        addJS('@@assetPath@@/js/app.js');

		function addJS(url) {
			var body = document.querySelector('head');
			var script = document.createElement('script');
			script.setAttribute('src', url);
			body.appendChild(script);
		}
    }

    return {
        init: init
    };
});