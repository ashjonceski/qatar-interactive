(function() {
	/* jshint validthis: true */
	'use strict';

	angular.module('qatarApp.itineraryItem.contentPanel', [])
		.directive('contentPanel', panel);

	panel.$inject = ['$state', '$timeout'];

	function panel($state, $timeout) {
		var directive = {
			restrict: 'EA',
			templateUrl: 'app.itinerary-item/content-panel/content-panel.template.html',
			scope: {
				itineraryItem: '=',
				navState: '=',
				toggleContent: '&',
				toggleState: '='
			},
			controller: Controller,
			controllerAs: 'vm',
			bindToController: true,
			link: linkFunc,
			replace: true
		};

		return directive;

		function linkFunc(scope, el, attr, ctrl) {
			scope.goToMain = goToMain;
			var clicked = false;

			$timeout(function(){
				angular.element(document.getElementById("book-now")).on('click', function(){
					if ( !clicked ){
						// track book now
						ga('glabsau.send', 'event', 'Book Now', $state.params.itinerary, 'Qatar Interactive 2016');
						clicked = true;
					}
				});
			}, 0);

			function goToMain(){
				angular.element(document.getElementById("qatar-app")).removeClass('main-to-itinerary itinerary-to-main itinerary-to-itinerary');
				angular.element(document.getElementById("qatar-app")).addClass('itinerary-to-main');

				$state.go('root');
				return;
			}
		}
	}

	Controller.$inject = ['$filter', '$scope', 'globals', 'services'];

	function Controller($filter, $scope, globals, services) {
		var vm = this;

		if ( vm.itineraryItem !== undefined ){
			vm.itineraryItemSlug = vm.itineraryItem.slug;
			vm.itineraryItemIntro = $filter('trustedHTML')(vm.itineraryItem.intro);
			vm.itineraryItemBody = $filter('trustedHTML')(vm.itineraryItem.body);
			vm.itineraryItemDetailWebsite = $filter('trustedHTML')(vm.itineraryItem.detailwebsite);
			vm.itineraryItemDetailExtra = $filter('trustedHTML')(vm.itineraryItem.detailextra);

			vm.bgimage = undefined;

			if ( vm.itineraryItem.bgimage !== '' ){
				lazyloadAsset(vm.itineraryItem.bgimage);

				$scope.$on('Viewport:Update', function(){
					lazyloadAsset(vm.itineraryItem.bgimage);
				});
			}
		}

		function lazyloadAsset(src) {
			if ( src === undefined ){
				// Do nothing
			} 

			var devicetype = globals.viewport.ismobile && globals.viewport.isshort ? 'mobile/' : 'desktop/';
			var output;
			
			if ( globals.assets['@@assetPath@@/' + devicetype + src] ){
				// get proloaded image if it exists
				vm.bgimage = globals.assets['@@assetPath@@/' + devicetype + src];
			} else {
				var img = services.lazyloadAsset([{
					src: '@@assetPath@@/' + devicetype + src
				}]);

				img.then(function(blobUrl) {
					vm.bgimage = blobUrl;
				}, function(reason) {
					// Do nothing
					console.log('Failed to lazyload image.');
				});
			}

			return;
		}
	}

})();
