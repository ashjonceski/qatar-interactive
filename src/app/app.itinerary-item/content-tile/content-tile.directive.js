(function() {
	/* jshint validthis: true */
	'use strict';

	angular.module('qatarApp.itineraryItem.contentTile', [])
		.directive('contentTile', tile);

	tile.$inject = [];

	function tile() {
		var directive = {
			restrict: 'EA',
			templateUrl: 'app.itinerary-item/content-tile/content-tile.template.html',
			scope: {
				tile: '=',
				classes: '@'
			},
			controller: Controller,
			controllerAs: 'vm',
			bindToController: true,
			link: linkFunc,
			replace: true
		};

		return directive;

		function linkFunc(scope, el, attr, ctrl) {

		}
	}

	Controller.$inject = ['$scope', 'globals', 'services'];

	function Controller($scope, globals, services) {
		var vm = this;

		if ( vm.tile !== undefined ){
			vm.bgimage = undefined;

			lazyloadAsset(vm.tile.bgimage);

			$scope.$on('Viewport:Update', function(){
				lazyloadAsset(vm.tile.bgimage);
			});
		}

		function lazyloadAsset(src) {
			if ( src === undefined ){
				// Do nothing
			} 

			var devicetype = globals.viewport.ismobile && globals.viewport.isshort ? 'mobile/' : 'desktop/';
			var output;
			
			if ( globals.assets['@@assetPath@@/' + devicetype + src] ){
				// get proloaded image if it exists
				vm.bgimage = globals.assets['@@assetPath@@/' + devicetype + src];
			} else {
				var img = services.lazyloadAsset([{
					src: '@@assetPath@@/' + devicetype + src
				}]);

				img.then(function(blobUrl) {
					vm.bgimage = blobUrl;
				}, function(reason) {
					// Do nothing
					console.log('Failed to lazyload image.');
				});
			}

			return;
		}
	}

})();
