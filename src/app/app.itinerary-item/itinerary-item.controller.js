(function() {
	/* jshint validthis: true */
	
	'use strict';

	angular
		.module('qatarApp.itineraryItem', [
			'qatarApp.itineraryItem.contentTile',
			'qatarApp.itineraryItem.contentPanel'
		])
		.controller('itineraryItemCtrl', Controller);

	Controller.$inject = ['$state', '$stateParams', '$scope', '$filter', '$window', '$timeout', 'globals', 'services'];

	function Controller($state, $stateParams, $scope, $filter, $window, $timeout, globals, services) {
		var vm = this;

		globals.showVideo = false;
		services.updateStorage('app.showVideo', globals.showVideo);

		var itemClass = 'item-' + $scope.navState.active.slug;
		var itemType = '';
		var container;
		var containerInner;
		var Acontainer;
		var expanded = false;
		vm.scrollPos = undefined;
		
		vm.toggleContent = toggleContent;
		vm.expandContent = false;
		vm.itemId = undefined;

		var scrolling = _.debounce($scope.changeItineraryItem, 200);

		var scrollStart = 0;
		var scrollEnd = 0;
		var scrollDir = undefined;

		var specialScrolling = _.debounce(function(event){
			if ( event.type === 'ps-y-reach-start' ){
				scrollStart++;
				if ( containerInner.clientHeight > container.clientHeight ){
					if ( scrollStart >= 2 ){
						$scope.changeItineraryItem({
							type: 'perfect-scroll-up'
						});
					}
				} else {
					if ( scrollStart >= 2 ){
						$scope.changeItineraryItem({
							type: 'perfect-scroll-up'
						});
					}
				}
			} else if ( event.type === 'ps-y-reach-end' ){
				scrollEnd++;
				if ( containerInner.clientHeight > container.clientHeight ){
					if ( scrollEnd >= 2 ){
						$scope.changeItineraryItem({
							type: 'perfect-scroll-down'
						});
					}
				} else {
					if ( scrollEnd >= 2 && scrollDir == 'down' ){
						$scope.changeItineraryItem({
							type: 'perfect-scroll-down'
						});	
					} else if ( scrollEnd >= 2 && scrollDir == 'up' ){
						$scope.changeItineraryItem({
							type: 'perfect-scroll-up'
						});	
					}
				}
			}
		}, 200);

		var clearScrolling = _.debounce(function(event){
			if ( event.type === 'ps-scroll-up' ){
				scrollEnd = 0;
				scrollDir = 'up';
			} else if ( event.type === 'ps-scroll-down' ){
				scrollStart = 0;
				scrollDir = 'down';
			} else {
				if ( event.detail > 0 || event.wheelDelta < 0 || event.deltaY > 0 ){
					scrollStart = 0;
					scrollDir = 'down';
				} else if ( event.detail < 0 || event.wheelDelta > 0 || event.deltaY < 0 ){
					scrollDir = 'up';
					if ( containerInner.clientHeight > container.clientHeight ){
						scrollEnd = 0;
					}
				}
			}
		}, 100);

		/* Else set up itinerary item */
		if ( $scope.itineraryItems !== undefined ){
			vm.itineraryItem = $scope.itineraryItems[$scope.navState.active.index];

			if ( vm.itineraryItem !== undefined ){
				if ( vm.itineraryItem.type === 'end' ){
					itemType = ' item-end';
					vm.itemId = 'item-end';
				}

				if ( $scope.itineraryItems[$scope.navState.next.index] && $scope.itineraryItems[$scope.navState.next.index].type === 'end' ){
					itemType = ' item-beforeend';
					vm.itemId = 'item-beforeend';
				}

				vm.itemSlug = vm.itineraryItem.slug;
				vm.itemClass = itemClass + itemType;

				$timeout(activate, 0);
			}
		}

		function activate() {
			var appWindow = angular.element($window);

			//var touch = new Hammer.Manager(window);
			var maptouch = new Hammer.Manager(document.getElementById('map-container-'+$scope.itinerarySlug));
			var tiletouch = new Hammer.Manager(document.getElementById('tile-container-'+vm.itemSlug));
			var contenttouch = new Hammer.Manager(document.getElementById('content-container-'+vm.itemSlug));
			var mapscroll = angular.element(document.getElementById('map-container-'+$scope.itinerarySlug));
			var tilescroll = angular.element(document.getElementById('tile-container-'+vm.itemSlug));
			//var contentscroll = angular.element(document.getElementById('content-container'));

			// Perfect Scrollbar
			container = document.getElementById('content-container-'+vm.itemSlug);
			containerInner = document.getElementById('content-container-inner-'+vm.itemSlug);
			Acontainer = angular.element(container);
			Ps.initialize(container, {
				suppressScrollX: true
			});

			// Swipe recognizer
			maptouch.add( new Hammer.Swipe() );
			tiletouch.add( new Hammer.Swipe() );
			contenttouch.add( new Hammer.Swipe() );

			if ( globals.viewport.ismobile && globals.viewport.isshort ){
				maptouch.get('swipe').set({ direction: Hammer.DIRECTION_HORIZONTAL });
				tiletouch.get('swipe').set({ direction: Hammer.DIRECTION_HORIZONTAL });
				contenttouch.get('swipe').set({ 
					direction: Hammer.DIRECTION_HORIZONTAL,
					enable: true,
					threshold: 150
				});

				updateSize();
			} else if ( globals.viewport.ismobile && !globals.viewport.isshort ){
				maptouch.get('swipe').set({ direction: Hammer.DIRECTION_VERTICAL });
				tiletouch.get('swipe').set({ direction: Hammer.DIRECTION_VERTICAL });
				
				if ( vm.itemId === 'item-end' ){
					contenttouch.get('swipe').set({ 
						direction: Hammer.DIRECTION_VERTICAL,
						enable: true,
						threshold: 150
					});
				} else if ( containerInner.clientHeight < container.clientHeight ){
					contenttouch.get('swipe').set({ 
						direction: Hammer.DIRECTION_VERTICAL,
						enable: true,
						threshold: 150
					});
				} else {
					contenttouch.get('swipe').set({ enable: false });
				}
			} else if ( !globals.viewport.ismobile ){
				maptouch.get('swipe').set({ direction: Hammer.DIRECTION_VERTICAL });
				tiletouch.get('swipe').set({ direction: Hammer.DIRECTION_VERTICAL });

				if ( vm.itemId === 'item-end' ){
					contenttouch.get('swipe').set({ 
						direction: Hammer.DIRECTION_VERTICAL,
						enable: true,
						threshold: 150
					});
				} else if ( containerInner.clientHeight < container.clientHeight ){
					contenttouch.get('swipe').set({ 
						direction: Hammer.DIRECTION_VERTICAL,
						enable: true,
						threshold: 150
					});
				} else {
					contenttouch.get('swipe').set({ enable: false });
				}
			}

			// Key Listener
			appWindow.on('keydown', scrolling);

			// Touch Listener
			maptouch.on('swipeleft swiperight swipeup swipedown', scrolling);
			tiletouch.on('swipeleft swiperight swipeup swipedown', scrolling);
			contenttouch.on('swipeleft swiperight swipeup swipedown', scrolling);
			
			// Scroll
			mapscroll.on('wheel DOMMouseScroll mousewheel onwheel', scrolling);
			tilescroll.on('wheel DOMMouseScroll mousewheel onwheel', scrolling);

			Acontainer.on('ps-y-reach-end ps-y-reach-start', specialScrolling);
			Acontainer.on('ps-scroll-up ps-scroll-down', clearScrolling);
			Acontainer.on('wheel DOMMouseScroll mousewheel onwheel', clearScrolling);

			$scope.$on('Viewport:Update', function(){
				if ( globals.viewport.ismobile && globals.viewport.isshort ){
					maptouch.get('swipe').set({ direction: Hammer.DIRECTION_HORIZONTAL });
					tiletouch.get('swipe').set({ direction: Hammer.DIRECTION_HORIZONTAL });
					contenttouch.get('swipe').set({ 
						direction: Hammer.DIRECTION_HORIZONTAL,
						enable: true,
						threshold: 150
					});

					updateSize();
				} else if ( globals.viewport.ismobile && !globals.viewport.isshort ){
					maptouch.get('swipe').set({ direction: Hammer.DIRECTION_VERTICAL });
					tiletouch.get('swipe').set({ direction: Hammer.DIRECTION_VERTICAL });
					
					if ( vm.itemId === 'item-end' ){
						contenttouch.get('swipe').set({ 
							direction: Hammer.DIRECTION_VERTICAL,
							enable: true,
							threshold: 150
						});
					} else if ( containerInner.clientHeight < container.clientHeight ){
						contenttouch.get('swipe').set({ 
							direction: Hammer.DIRECTION_VERTICAL,
							enable: true,
							threshold: 150
						});
					} else {
						contenttouch.get('swipe').set({ enable: false });
					}
				} else if ( !globals.viewport.ismobile ){
					maptouch.get('swipe').set({ direction: Hammer.DIRECTION_VERTICAL });
					tiletouch.get('swipe').set({ direction: Hammer.DIRECTION_VERTICAL });

					if ( vm.itemId === 'item-end' ){
						contenttouch.get('swipe').set({ 
							direction: Hammer.DIRECTION_VERTICAL,
							enable: true,
							threshold: 150
						});
					} else if ( containerInner.clientHeight < container.clientHeight ){
						contenttouch.get('swipe').set({ 
							direction: Hammer.DIRECTION_VERTICAL,
							enable: true,
							threshold: 150
						});
					} else {
						contenttouch.get('swipe').set({ enable: false });
					}
				}

				// Reset scroll counters
				scrollStart = 0;
				scrollEnd = 0;

				// Update Perfect Scroll
				Ps.update(container);
			});

			$scope.$on("$destroy", function() {
				// Key Listener
				appWindow.off('keydown', scrolling);

				// Touch Listener
				maptouch.off('swipeleft swiperight swipeup swipedown', scrolling);
				tiletouch.off('swipeleft swiperight swipeup swipedown', scrolling);
				contenttouch.off('swipeleft swiperight swipeup swipedown', scrolling);
				
				// Scroll
				mapscroll.off('wheel DOMMouseScroll mousewheel onwheel', scrolling);
				tilescroll.off('wheel DOMMouseScroll mousewheel onwheel', scrolling);

				Acontainer.off('ps-y-reach-end ps-y-reach-start', specialScrolling);
				Acontainer.off('ps-scroll-up ps-scroll-down', clearScrolling);
				Acontainer.off('wheel DOMMouseScroll mousewheel onwheel', clearScrolling);

				// Destroy Perfect Scroll
				Ps.destroy(container);
			});

			Ps.update(container);
		}

		function toggleContent(){
			vm.expandContent = vm.expandContent ? false : true;
			if ( vm.expandContent ){
				vm.itemClass = itemClass + itemType + ' open';

				if ( !expanded ){
					ga('glabsau.send', 'event', 'Mobile', 'expand: /#/' + $state.params.itinerary + '/' + $state.params.stop, 'Qatar Interactive 2016');
					expanded = true;
				}
			} else {
				vm.itemClass = itemClass + itemType;
			}
			return;
		}

		function updateSize(){
			var height = angular.element(document.getElementById('container-itinerary')).prop('offsetHeight');
			angular.element(document.getElementById('tile-container-' + $scope.navState.active.slug)).attr('style', 'padding-bottom: ' + Math.floor(height/2) + 'px;');
			angular.element(document.getElementById('tile1')).attr('style', 'height: ' + Math.floor(height/2) + 'px;');
			return;
		}
	}

})();