(function() {
	'use strict';

	angular
		.module('qatarApp', [
			'ngAnimate',
			'ui.router',
			'qatarApp.preloader',
			'qatarApp.video',
			'qatarApp.main',
			'qatarApp.itinerary',
			'qatarApp.itineraryItem',
			'qatarApp.services',
			'qatarApp.filters',
			'qatarApp.404'
		])
		.constant('createjs', createjs)
		.constant('_', _)
		.constant('L', L)
		.constant('Hammer', Hammer)
		.constant('Ps', Ps)
		.constant('blobUtil', blobUtil)
		.constant('ga', ga);

})();