(function() {
	/* jshint validthis: true */
	
	'use strict';

	angular
		.module('qatarApp.filters', [])
		.filter('trustedURL', ['$sce', function ($sce) {
			return function(url) {
				return $sce.trustAsResourceUrl(url);
			};
		}])
		.filter('trustedHTML', ['$sce', function ($sce) {
			return function(html) {
				return $sce.trustAsHtml(html);
			};
		}])
		.filter('lazyloadAsset', ['globals', 'services', function (globals, services) {
			return function(src) {
				if ( src === undefined ){
					// Do nothing
					return src;
				} 

				var devicetype = globals.viewport.ismobile && globals.viewport.isshort ? 'mobile/' : 'desktop/';
				var output;
				
				if ( globals.assets['@@assetPath@@/' + devicetype + src] ){
					// get proloaded image if it exists
					return globals.assets['@@assetPath@@/' + devicetype + src];
				} else {
					var img = services.lazyloadAsset([{
						src: '@@assetPath@@/' + devicetype + src
					}]);

					img.then(function(blobUrl) {
						return blobUrl;
					}, function(reason) {
						// Do nothing
						console.log('Failed to lazyload image.');
						return undefined;
					});
				}
			};
		}]);

})();