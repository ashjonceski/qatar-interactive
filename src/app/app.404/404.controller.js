(function() {
	/* jshint validthis: true */
	
	'use strict';

	angular
		.module('qatarApp.404', [])
		.controller('notFound', Controller);

	Controller.$inject = [];

	function Controller() {
		var vm = this;

		vm.pageClass = 'page-not-found';
	}

})();