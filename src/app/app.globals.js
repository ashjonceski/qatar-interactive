(function() {
	'use strict';

	angular
		.module('qatarApp')
		.value('globals', {
			preloaded: false,
			preloadProgress: 0,
			data: undefined,
			assets: undefined,
			viewport: {
				ismobile: undefined,
				isshort: undefined,
				isfireox: undefined,
				height: undefined,
				width: undefined,
				orientation: undefined
			},
			showVideo: true,
			showHint: true
		});

})();