(function() {
	'use strict';

	angular
		.module('qatarApp')
		.config(function ($stateProvider, $urlRouterProvider, $locationProvider, $urlMatcherFactoryProvider) {

			$urlMatcherFactoryProvider.strictMode(false);

			$stateProvider
				.state('root', {
					url: '/',
					templateUrl: 'app.main/main.template.html',
					controller: 'mainCtrl',
					controllerAs: 'vm',
					resolve: {
						preload: preload,
						configApp: configApp
					}
				})
				.state('itinerary', {
					abstract: true,
					url: '/:itinerary',
					templateUrl: 'app.itinerary/itinerary.template.html',
					controller: 'itineraryCtrl',
					controllerAs: 'vm',
					resolve: {
						preload: preload,
						configApp: configApp
					}
				})
				.state('itinerary.stop', {
					url: '/:stop',
					templateUrl: 'app.itinerary-item/itinerary-item.template.html',
					controller: 'itineraryItemCtrl',
					controllerAs: 'vm'
				})
				.state('404', {
					templateUrl: 'app.404/404.template.html',
					controller: 'notFound',
					controllerAs: 'vm',
					resolve: {
						preload: preload,
						configApp: configApp
					}
				});

			$urlRouterProvider.otherwise(function($injector, $location) {
				var $state = $injector.get('$state');

				if ( $location.$$path !== '' ){
					/* Any unmapped URL */
					$state.go('404', null, {
						location: false
					});
				} else {
					/* Bare #! */
					$state.go('root');
				}
			});
		});

	function preload($state, $rootScope, services, globals) {
		/* Video Show */
		var showVideo = services.getStorage('app.showVideo');
		var showHint = services.getStorage('app.showHint');

		if ( showVideo !== null ){
			globals.showVideo = showVideo;
		} else {
			services.updateStorage('app.showVideo', globals.showVideo);
			showVideo = globals.showVideo;
		}

		if ( showHint !== null ){
			globals.showHint = showHint;
		} else {
			services.updateStorage('app.showHint', globals.showHint);
			showHint = globals.showHint;
		}

		if ( !globals.preloaded ){
			var device;

			if ( globals.viewport.ismobile && globals.viewport.isshort ){
				/* Mobile asset manifest */
				device = 'mobile';
			} else {
				/* Tablet & Desktop asset manifest */
				device = 'desktop';
			}

			var manifest = {
				"manifest": [
					{"src": "https://interactive.guim.co.uk/spreadsheetdata/1NF9Ka-krvlNE6NSwGR6dvZlCREln0LM6Uzh5R7ovx8Q.json", "id":"itineraries"},
					{"src": "https://interactive.guim.co.uk/spreadsheetdata/1qLp1fWI3V0T0SEvnqv7mrLNj-w2FiIj8ntkVOPNztMM.json", "id":"stops"},
					{"src": "https://interactive.guim.co.uk/spreadsheetdata/1PIKVM9gqnzTapi1G3wX5pyHZV1HkeFR0lx859_OzqhA.json", "id":"tiles"}
				]
			};

			if ( !globals.viewport.ismobile && !globals.viewport.isshort ){
				manifest.manifest.push({"src": "@@assetPath@@/" + device + "/imgs/london-hero.jpg", "type": createjs.AbstractLoader.IMAGE});
				manifest.manifest.push({"src": "@@assetPath@@/" + device + "/imgs/paris-hero.jpg", "type": createjs.AbstractLoader.IMAGE});
				manifest.manifest.push({"src": "@@assetPath@@/" + device + "/imgs/barcelona-hero.jpg", "type": createjs.AbstractLoader.IMAGE});
				manifest.manifest.push({"src": "@@assetPath@@/" + device + "/imgs/rome-hero.jpg", "type": createjs.AbstractLoader.IMAGE});

				if ( globals.viewport.isfirefox && showVideo ){
					manifest.manifest.push({"src": "@@assetPath@@/" + device + "/vids/introvideo.webm", "type": createjs.AbstractLoader.VIDEO});
				} else if ( showVideo ) {
					manifest.manifest.push({"src": "@@assetPath@@/" + device + "/vids/introvideo.mp4", "type": createjs.AbstractLoader.VIDEO});
				}
			}

			var preload = services.preloadAssets(manifest);
			preload.then(function(result) {
				// Success
				globals.preloaded = true;
				globals.data = result.data;
				globals.assets = result.assetMap;
				$rootScope.$emit('Preload:Complete');
				console.log('Preload Complete');
			}, function() {
				// Fail
				$rootScope.$emit('Preload:Failed');
				console.log('Preload Failed');
			}, function(progress) {
				// Update
				globals.preloadProgress = Math.round(progress*100);
				$rootScope.$emit('Preload:Update', progress);
			});

			return preload;
		} else if ( globals.data !== undefined && globals.assets !== undefined ) {
			console.log('Already Preloaded');
			return true;
		} else {
			console.log('Preload Error');
			return false;
		}
	}

	function configApp(services, globals) {
		/* Video Show */
		var showVideo = services.getStorage('app.showVideo');
		var showHint = services.getStorage('app.showHint');

		if ( showVideo !== null ){
			globals.showVideo = showVideo;
		} else {
			services.updateStorage('app.showVideo', globals.showVideo);
		}

		if ( showHint !== null ){
			globals.showHint = showHint;
		} else {
			services.updateStorage('app.showHint', globals.showHint);
		}

		return true;
	}

	function trackPage() {
		
		
		return true;
	}

})();