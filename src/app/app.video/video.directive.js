(function() {
	/* jshint validthis: true */
	'use strict';

	angular.module('qatarApp.video', [])
		.directive('video', video);

	video.$inject = ['$timeout', 'globals', 'services'];

	function video($timeout, globals, services) {
		var directive = {
			restrict: 'EA',
			templateUrl: 'app.video/video.template.html',
			scope: {
				showVideo: '='
			},
			controller: Controller,
			controllerAs: 'vm',
			bindToController: true,
			link: linkFunc,
			replace: true
		};

		return directive;

		function linkFunc(scope, el, attr, ctrl) {
			$timeout(init, 0);

			function init() {
				var autoplay = globals.viewport.ismobile && globals.viewport.isshort ? '' : 'autoplay ';
				var videoEl = '<video width="320" height="240" preload="auto" ' + autoplay + 'poster="' + ctrl.poster + '" src="' + ctrl.video + '" type="' + ctrl.type + '">Your browser does not support the video tag.</video>';
				angular.element(document.getElementById('video-container')).append(videoEl);
				var video = angular.element(el).find('video');
				var muted = false;

				var myElement = document.getElementById('video-play');
				var mc = new Hammer.Manager(myElement);
				mc.add( new Hammer.Tap() );
				mc.add( new Hammer.Press() );

				// track video shown
				ga('glabsau.send', 'event', 'Video', 'Shown', 'Qatar Interactive 2016');

				scope.playVideo = playVideo;
				scope.hideVideo = hideVideo;
				scope.muteVideo = muteVideo;
				scope.skipVideo = skipVideo;

				video.bind('play', function(){
					ctrl.playing = true;
					scope.$apply();
				});

				video.bind('playing', function(){
					//console.log('playing');
					ctrl.playing = true;
					scope.$apply();
				});

				video.bind('ended', function(){
					// track video completed
					ga('glabsau.send', 'event', 'Video', 'Watched', 'Qatar Interactive 2016');

					hideVideo();
					scope.$apply();
				});

				mc.on("tap press", function(event) {
					playVideo();
				});

				function playVideo() {
					video[0].play();

					// track video explicitly played
					ga('glabsau.send', 'event', 'Video', 'Played', 'Qatar Interactive 2016');
					return;
				}

				function hideVideo() {
					video[0].pause();
					ctrl.showVideo = false;
					services.updateStorage('app.showVideo', false);
					globals.showVideo = false;
					return;
				}

				function skipVideo() {
					hideVideo();

					// track video skipped
					ga('glabsau.send', 'event', 'Video', 'Skipped', 'Qatar Interactive 2016');
					return;
				}

				function muteVideo() {
					if ( !muted ){
						muted = true;
						
						// track video skipped
						ga('glabsau.send', 'event', 'Video', 'Muted', 'Qatar Interactive 2016');
					}

					video[0].muted = !video[0].muted ? true : false;
					ctrl.muted = !ctrl.muted ? true : false;
					return;
				}

				scope.$on("$destroy", function() {
					video.unbind('play', function(){ return; });
					video.bind('playing', function(){ return; });
					video.bind('ended', function(){ return; });
					mc.off("tap press", function(event) { return; });
				});
			}
		}
	}

	Controller.$inject = ['$filter', 'globals'];

	function Controller($filter, globals) {
		var vm = this;
		var devicetype = globals.viewport.ismobile && globals.viewport.isshort ? 'mobile/' : 'desktop/';

		vm.poster = '@@assetPath@@/' + devicetype + 'imgs/introvideo-poster.jpg';

		if ( globals.viewport.isfirefox ){
			if ( globals.assets['@@assetPath@@/' + devicetype + 'vids/introvideo.webm'] ){
				// get proloaded image if it exists
				vm.video = globals.assets['@@assetPath@@/' + devicetype + 'vids/introvideo.webm'];
			} else {
				vm.video = '@@assetPath@@/' + devicetype + 'vids/introvideo.webm';
			}
			vm.type = 'video/webm';
		} else {
			if ( globals.assets['@@assetPath@@/' + devicetype + 'vids/introvideo.mp4'] ){
				// get proloaded image if it exists
				vm.video = globals.assets['@@assetPath@@/' + devicetype + 'vids/introvideo.mp4'];
			} else {
				vm.video = '@@assetPath@@/' + devicetype + 'vids/introvideo.mp4';
			}
			vm.type = 'video/mp4';
		}

		vm.muted = false;
		vm.playing = false;
	}

})();
