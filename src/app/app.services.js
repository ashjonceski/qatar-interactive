(function() {
	/* jshint validthis: true */
	
	'use strict';

	angular
		.module('qatarApp.services', [])
		.service('services', Service);

	Service.$inject = ['$q', '$timeout', 'globals'];

	function Service($q, $timeout, globals) {

		return {
			preloadAssets: preloadAssets,
			lazyloadAsset: lazyloadAsset,
			updateStorage: updateStorage,
			getStorage: getStorage
		};

		function config() {
			return true;
		}

		function preloadAssets(manifest) {
			var deferred = $q.defer();
			var result = {
				data: {},
				assetMap: {}
			};

			var preloader = new createjs.LoadQueue(true);
			preloader.loadManifest(manifest);

			preloader.on('progress', function(event) {
				deferred.notify(event.loaded);
			});

			preloader.on('complete', function() {
				$timeout(function(){
					deferred.resolve(result);
				}, 800);
			});

			preloader.on('fileload', function(event) {
				if ( event.item.type !== 'json' ){
					result.assetMap[event.item.id] = blobUtil.createObjectURL(event.rawResult);
				} else {
					result.data[event.item.id] = event.result.sheets.Sheet1;
				}
			});

			preloader.on('error', function(event) {
				deferred.reject();
			});

			return deferred.promise;
		}

		function lazyloadAsset(item) {
			var deferred = $q.defer();
			var result = undefined;

			var lazyloader = new createjs.LoadQueue(true);
			lazyloader.loadManifest(item);

			lazyloader.on('complete', function() {
				deferred.resolve(result);
			});

			lazyloader.on('fileload', function(event) {
				result = blobUtil.createObjectURL(event.rawResult);
				globals.assets[event.item.id] = result;
			});

			lazyloader.on('error', function(event) {
				deferred.reject();
			});

			return deferred.promise;
		}

		function updateStorage(key, value) {
			if ( typeof(window.sessionStorage) !== undefined ) {
				window.sessionStorage.setItem( key, JSON.stringify(value) );
			}
			return;
		}

		function getStorage(key) {
			if ( typeof(window.localStorage) !== undefined ) {
				if ( window.sessionStorage.getItem(key) !== undefined || window.sessionStorage.getItem(key) !== null ){
					if ( window.sessionStorage.getItem(key) === 'true' ){
						return true;
					} else if ( window.sessionStorage.getItem(key) === 'false' ){
						return false;
					} else {
						return window.sessionStorage.getItem(key);
					}
				}
			}
			return undefined;
		}

	}

})();