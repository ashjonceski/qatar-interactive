(function() {
	/* jshint validthis: true */
	'use strict';

	angular.module('qatarApp')
		.directive('app', app);

	app.$inject = ['$window', 'globals', '$rootScope'];

	function app($window, globals, $rootScope) {
		var directive = {
			restrict: 'EA',
			templateUrl: 'app.template.html',
			scope: {},
			controller: Controller,
			controllerAs: 'vm',
			bindToController: true,
			link: linkFunc,
			replace: true
		};

		return directive;

		function linkFunc(scope, el, attr, ctrl) {
			/* On load */
			globals.viewport = updateViewport();
			ctrl.viewport = globals.viewport;

			/* On resize */
			angular.element($window).on('resize', _.debounce(function() {
				globals.viewport = updateViewport();
				ctrl.viewport = globals.viewport;
				$rootScope.$broadcast('Viewport:Update');
				scope.$apply();
			}, 200));

			function updateViewport() {
				return {
					width: $window.innerWidth,
					height: $window.innerHeight,
					orientation: $window.innerWidth > $window.innerHeight ? 'landscape' : 'portrait',
					ismobile: $window.innerWidth < 780 ? true : false,
					isshort: $window.innerHeight < 900 ? true : false,
					isfirefox: navigator.userAgent.indexOf("Firefox") > 0 ? true : false
				};
			}
		}
	}

	Controller.$inject = ['$rootScope', '$location', '$window', 'globals'];

	function Controller($rootScope, $location, $window, globals) {
		var vm = this;

		vm.preloadProgress = globals.preloadProgress;
		vm.preloaded = globals.preloaded;
		vm.viewport = globals.viewport;

		$rootScope.$on('Preload:Update', function(){
			vm.preloadProgress = globals.preloadProgress;
		});

		$rootScope.$on('Preload:Complete', function(){
			vm.preloaded = globals.preloaded;
		});

		// track pageview on state change
        $rootScope.$on('$stateChangeSuccess', function (event) {
        	ga('glabsau.set', 'page', $window.location.pathname + '#' + $location.url());
			ga('glabsau.send', 'pageview');
        });
	}

})();
