(function() {
	/* jshint validthis: true */
	'use strict';

	angular.module('qatarApp.preloader', [])
		.directive('preloader', preloader);

	preloader.$inject = [];

	function preloader() {
		var directive = {
			restrict: 'EA',
			templateUrl: 'app.preloader/preloader.template.html',
			scope: {
				preloadProgress: '=',
				preloaded: '='
			},
			controller: Controller,
			controllerAs: 'vm',
			bindToController: true,
			link: linkFunc,
			replace: false
		};

		return directive;

		function linkFunc(scope, el, attr, ctrl) {

		}
	}

	Controller.$inject = ['globals'];

	function Controller(globals) {
		var vm = this;

		var devicetype = globals.viewport.ismobile && globals.viewport.isshort ? 'mobile/' : 'desktop/';
		vm.bgimage = '@@assetPath@@/' + devicetype + 'imgs/introvideo-poster.jpg';
	}

})();
