(function() {
	/* jshint validthis: true */
	'use strict';

	angular.module('qatarApp.itinerary.topNavItem', [])
		.directive('topNavItem', navItem);

	navItem.$inject = [];

	function navItem() {
		var directive = {
			restrict: 'EA',
			templateUrl: 'app.itinerary/top-nav-item/top-nav-item.template.html',
			scope: {
				active: '=',
				itinerary: '=',
				navToggle: '&'
			},
			controller: Controller,
			controllerAs: 'vm',
			bindToController: true,
			link: linkFunc,
			replace: true
		};

		return directive;

		function linkFunc(scope, el, attr, ctrl) {

		}
	}

	Controller.$inject = ['$scope', '$state', 'globals', 'services'];

	function Controller($scope, $state, globals, services) {
		var vm = this;

		vm.bgimage = undefined;
		vm.goToItinerary = goToItinerary;

		lazyloadAsset(vm.itinerary.bgimage);

		$scope.$on('Viewport:Update', function(){
			lazyloadAsset(vm.itinerary.bgimage);
		});

		function lazyloadAsset(src) {
			if ( src === undefined ){
				// Do nothing
			} 

			var devicetype = globals.viewport.ismobile && globals.viewport.isshort ? 'mobile/' : 'desktop/';
			var output;
			
			if ( globals.assets['@@assetPath@@/' + devicetype + src] ){
				// get proloaded image if it exists
				vm.bgimage = globals.assets['@@assetPath@@/' + devicetype + src];
			} else {
				var img = services.lazyloadAsset([{
					src: '@@assetPath@@/' + devicetype + src
				}]);

				img.then(function(blobUrl) {
					vm.bgimage = blobUrl;
				}, function(reason) {
					// Do nothing
					console.log('Failed to lazyload image.');
				});
			}

			return;
		}

		function goToItinerary(itinerary, stop){
			angular.element(document.getElementById("qatar-app")).removeClass('main-to-itinerary itinerary-to-main').addClass('itinerary-to-itinerary');

			$state.go('itinerary.stop', {
				itinerary: itinerary,
				stop: stop
			});

			return;
		}
	}

})();
