(function() {
	/* jshint validthis: true */
	'use strict';

	angular.module('qatarApp.itinerary.hint', [])
		.directive('hint', hint);

	hint.$inject = [];

	function hint() {
		var directive = {
			restrict: 'EA',
			templateUrl: 'app.itinerary/hint/hint.template.html',
			scope: {
				showHint: '=',
				showVideo: '='
			},
			controller: Controller,
			controllerAs: 'vm',
			bindToController: true,
			link: linkFunc,
			replace: true
		};

		return directive;

		function linkFunc(scope, el, attr, ctrl) {

		}
	}

	Controller.$inject = ['globals'];

	function Controller(globals) {
		var vm = this;
	}

})();
