(function() {
	/* jshint validthis: true */
	
	'use strict';

	angular
		.module('qatarApp.itinerary', [
			'qatarApp.itinerary.map',
			'qatarApp.itinerary.topNavItem',
			'qatarApp.itinerary.dotNav',
			'qatarApp.itinerary.socialShare',
			'qatarApp.itinerary.hint'
		])
		.controller('itineraryCtrl', Controller);

	Controller.$inject = ['$state', '$scope', '$filter', '$window', 'globals', 'services'];

	function Controller($state, $scope, $filter, $window, globals, services) {
		var vm = this;
		var itineraries, pastIndex;

		vm.itineraries = globals.data.itineraries;
		vm.itineraryItems = undefined;
		vm.itineraryTiles = undefined;
		vm.pageClass = globals.showVideo ? 'page-itinerary video-playing' : 'page-itinerary';
		vm.showVideo = globals.showVideo;
		vm.showHint = globals.showHint;
		vm.itinerarySlug = $state.params.itinerary;
		vm.itineraryItemSlug = $state.params.stop;
		vm.mapPoints = [];
		vm.showNav = false;
		vm.navToggle = navToggle;
		
		$scope.navState = {
			active: {
				index: undefined,
				slug: undefined
			},
			prev: {
				index: undefined,
				slug: undefined
			},
			next: {
				index: undefined,
				slug: undefined
			}
		};

		/* Find itinerary */
		if ( vm.itinerarySlug !== '' ){
			itineraries = $filter('filter')(vm.itineraries, {
				slug: vm.itinerarySlug
			}, true);
		}

		/* 404 if none found */
		if ( vm.itinerarySlug === undefined || itineraries === undefined || itineraries.length === 0 ){
			$state.go('404', null, { location: false });
			return;
		}

		/* Itinerary scope */
		vm.itinerary = itineraries[0];
		vm.itineraryId = vm.itinerary.itineraryid;
		vm.mapCentre = [ parseFloat(vm.itinerary.centrelat), parseFloat(vm.itinerary.centrelng) ];

		/* Find itinerary items/stops */
		vm.itineraryItems = $filter('filter')(globals.data.stops, {
			itineraryid: vm.itineraryId
		}, true);

		/* 404 if defined item not found */
		if ( vm.itineraryItems.length === 0 ){
			$state.go('404', null, { location: false });
			return;
		}

		if ( vm.itineraryItemSlug !== undefined || vm.itineraryItemSlug !== '' ){
			if ( _.find(vm.itineraryItems, { 'slug': vm.itineraryItemSlug }) === undefined ){
				$state.go('404', null, { location: false });
				return;
			}
		} else {
			var item = _.find(vm.itineraryItems, { 'order': '1' });
			vm.itineraryItemSlug = item.slug;
		}

		vm.itineraryItems = $filter('orderBy')(vm.itineraryItems, 'order');	
		vm.itineraryItemCount = vm.itineraryItems.length;

		/* Process itinerary items/stops */
		if ( vm.itineraryItemCount > 0 ){
			_.forEach(vm.itineraryItems, function(val, index){	
				if ( val.slug === vm.itineraryItemSlug ){
					pastIndex = index;
					$scope.navState = updateNavState(index);
				}

				if ( val.type !== "end" ){
					if ( val.tileslot1 && !_.isObject(val.tileslot1) ){
						vm.itineraryItems[index].tileslot1 = _.find(globals.data.tiles, { 'tileid': val.tileslot1 });
					}
					if ( val.tileslot2 && !_.isObject(val.tileslot2) ){
						vm.itineraryItems[index].tileslot2 = _.find(globals.data.tiles, { 'tileid': val.tileslot2 });
					}
					if ( val.tileslot3 && !_.isObject(val.tileslot3) ){
						vm.itineraryItems[index].tileslot3 = _.find(globals.data.tiles, { 'tileid': val.tileslot3 });
					}

					vm.mapPoints.push({
						index: index,
						coords: [ parseFloat(val.pointlat), parseFloat(val.pointlng) ],
						label: val.pointlabel,
						slug: val.slug
					});
				}

				if ( vm.itineraryItems[index + 1] && vm.itineraryItems[index + 1].type === "end" ){
					vm.itineraryItems[index].transition = false;
				}
			});
		}

		/* Make scope available to itinerary item/stop */
		$scope.itineraryItems = vm.itineraryItems;
		$scope.itinerarySlug = vm.itinerarySlug;
		$scope.itineraryItemSlug = vm.itineraryItemSlug;

		vm.change = change;
		$scope.changeItineraryItem = changeItineraryItem;

		activate();

		function activate() {
			$scope.$watch('vm.showVideo', function(){
				vm.pageClass = globals.showVideo ? 'page-itinerary video-playing' : 'page-itinerary';
				vm.showVideo = globals.showVideo;
			});

			/* Watch for state changes */
			$scope.$on('$stateChangeSuccess', function() {
				var index = _.findIndex(vm.itineraryItems, { 'slug': $state.params.stop });

				if ( pastIndex !== index && index > -1 ){
					pastIndex = index;
					$scope.navState = updateNavState(index);
					vm.itineraryItemSlug = $state.params.stop;
					$scope.itineraryItemSlug = $state.params.stop;
				}
			});
		}

		function updateNavState(index){
			index = parseFloat(index);
			var prev = index > 0 ? index - 1 : undefined;
			var next = index < (vm.itineraryItemCount - 1) ? index + 1 : undefined;

			var result = {
				active: {
					index: index,
					slug: vm.itineraryItems[index].slug
				},
				prev: {
					index: prev,
					slug: prev !== undefined ? vm.itineraryItems[prev].slug : undefined
				},
				next: {
					index: next,
					slug: next !== undefined ? vm.itineraryItems[next].slug : undefined
				}
			};

			return result;
		}

		function changeItineraryItem(event){
			switch (event.type){
				case 'perfect-scroll-down':
					//console.log('ps-down');
					if ( $scope.navState.next.slug !== undefined ){
						// Swipe Left
						vm.change(vm.itinerarySlug, $scope.navState.next.slug, 'direction-next', 'direction-prev', $scope.navState.next.index);
					}
					break;
				case 'perfect-scroll-up':
					//console.log('ps-up');
					if ( $scope.navState.prev.slug !== undefined ){
						// Swipe Right
						vm.change(vm.itinerarySlug, $scope.navState.prev.slug, 'direction-prev', 'direction-next', $scope.navState.prev.index);
					}
					break;
				case 'swipeleft':
					//console.log('swipe-left');
					if ( $scope.navState.next.slug !== undefined ){
						// Swipe Left
						vm.change(vm.itinerarySlug, $scope.navState.next.slug, 'direction-next', 'direction-prev', $scope.navState.next.index);
					}
					break;
				case 'swiperight':
					//console.log('swipe-right');
					if ( $scope.navState.prev.slug !== undefined ){
						// Swipe Right
						vm.change(vm.itinerarySlug, $scope.navState.prev.slug, 'direction-prev', 'direction-next', $scope.navState.prev.index);
					}
					break;
				case 'swipeup':
					//console.log('swipe-up');
					if ( $scope.navState.next.index !== undefined ){
						// Swipe Left
						vm.change(vm.itinerarySlug, $scope.navState.next.slug, 'direction-next', 'direction-prev', $scope.navState.next.index);
					}
					break;
				case 'swipedown':
					//console.log('swipe-down');
					if ( $scope.navState.prev.index !== undefined ){
						// Swipe Right
						vm.change(vm.itinerarySlug, $scope.navState.prev.slug, 'direction-prev', 'direction-next', $scope.navState.prev.index);
					}
					break;
				case 'keydown':
					//console.log('key');
					// Keyboard
					if ( !globals.viewport.ismobile ){
						if ( event.keyCode === 40 && $scope.navState.next.slug !== undefined ){
							vm.change(vm.itinerarySlug, $scope.navState.next.slug, 'direction-next', 'direction-prev', $scope.navState.next.index);
						} else if ( event.keyCode === 38 && $scope.navState.prev.slug !== undefined ){
							vm.change(vm.itinerarySlug, $scope.navState.prev.slug, 'direction-prev', 'direction-next', $scope.navState.prev.index);
						}
					}
					break;
				default:
					//console.log('wheel');
					// Mouse Wheel
					if ( !globals.viewport.ismobile ){
						if ( (event.detail > 0 || event.wheelDelta < 0 || event.deltaY > 0) && $scope.navState.next.slug !== undefined ){
							//scroll down
							vm.change(vm.itinerarySlug, $scope.navState.next.slug, 'direction-next', 'direction-prev', $scope.navState.next.index);
						} else if ( (event.detail < 0 || event.wheelDelta > 0 || event.deltaY < 0) && $scope.navState.prev.slug !== undefined ) {
							//scroll up
							vm.change(vm.itinerarySlug, $scope.navState.prev.slug, 'direction-prev', 'direction-next', $scope.navState.prev.index);
						}
					}
					break;
			}			
			return;
		}

		function change(itinerary, stop, addClass, removeClass, index){
			if ( $scope.navState.active.index !== index ){
				if ( addClass === undefined || removeClass === undefined ){
					if ( $scope.navState.active.index > index ){
						addClass = 'direction-prev';
						removeClass = 'direction-next';
					} else if ( index > $scope.navState.active.index ){
						addClass = 'direction-next';
						removeClass = 'direction-prev';
					}
				}
				
				angular.element(document.getElementById("qatar-app")).removeClass(removeClass).addClass(addClass);
				angular.element(document.getElementById("container-itinerary")).removeClass('transition-tile1 transition-tile2 transition-tile3');

				var current = vm.itineraryItems[$scope.navState.active.index];
				var next = vm.itineraryItems[index];

				if ( next !== undefined && current !== undefined ){
					if ( current.type !== 'end' && next.type !== 'end'){
						if ( current.tileslot1.tileid !== next.tileslot1.tileid ){
							angular.element(document.getElementById("container-itinerary")).addClass('transition-tile1');
						}

						if ( current.tileslot2.tileid !== next.tileslot2.tileid ){
							angular.element(document.getElementById("container-itinerary")).addClass('transition-tile2');
						}

						if ( current.tileslot3.tileid !== next.tileslot3.tileid ){
							angular.element(document.getElementById("container-itinerary")).addClass('transition-tile3');
						}
					}
				}

				if ( vm.showHint ){
					services.updateStorage('app.showHint', false);
					globals.showHint = false;
					vm.showHint = false;
				}

				$state.go('itinerary.stop', {
					itinerary: itinerary,
					stop: stop
				});
			}

			return;
		}

		function navToggle(bool){
			if ( bool !== undefined ){
				vm.showNav = bool;
			} else {
				vm.showNav = vm.showNav ? false : true;
			}
			return;
		}
	}

})();