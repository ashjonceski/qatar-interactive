(function() {
	/* jshint validthis: true */
	'use strict';

	angular.module('qatarApp.itinerary.map', [])
		.directive('map', map);

	map.$inject = ['$timeout', 'globals'];

	function map($timeout, globals) {
		var directive = {
			restrict: 'EA',
			templateUrl: 'app.itinerary/map/map.template.html',
			scope: {
				active: '=',
				centre: '=',
				points: '=',
				slug: '=',
				change: '&'
			},
			controller: Controller,
			controllerAs: 'vm',
			bindToController: true,
			link: linkFunc,
			replace: true
		};

		return directive;

		function linkFunc(scope, el, attr, ctrl) {	
			$timeout(init, 0);

			function init() {
				var map, width, height, activeIndex, markers = [];
				var mapname = 'itinerary-map-'+ctrl.slug;	
				//updateSize();

				var map = L.map(mapname, {
					minZoom: 16,
					maxZoom: 16,
					scrollWheelZoom: false,
					dragging: false,
					touchZoom: false,
					doubleClickZoom: false,
					boxZoom: false,
					keyboard: false
				}).setView(ctrl.centre, 16);

				L.tileLayer('https://cartodb-basemaps-{s}.global.ssl.fastly.net/dark_all/{z}/{x}/{y}.png', {
					attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a> &copy; <a href="http://cartodb.com/attributions">CartoDB</a>'
				}).addTo(map).on('load', function(event){
					angular.element(el).removeClass('loading');
					map.invalidateSize();
				});
				
				_.forEach(ctrl.points, function(val, index){
					var marker;
					var defaultIcon = L.divIcon({
						className: 'itinerary-map-marker',
						html: '<div class="index">' + Math.abs(index + 1) + '</div><svg viewBox="0 0 20.6 26.24"><path d="M10.3,0A10.28,10.28,0,0,0,0,10.26s0,0,0,0a11.63,11.63,0,0,0,.85,4.15A35.44,35.44,0,0,0,10.3,26.24a35.19,35.19,0,0,0,9.45-11.87,11.63,11.63,0,0,0,.85-4.15A10.26,10.26,0,0,0,10.3,0h0Zm0,18.18a8,8,0,1,1,8-8h0A8,8,0,0,1,10.3,18.18Z" transform="translate(0 0)"/></svg><div class="label">' + val.label + '<div class="arrow"></div></div>'
					});

					var activeIcon = L.divIcon({
						className: 'itinerary-map-marker-active',
						html: '<div class="index">' + Math.abs(ctrl.active.index + 1) + '</div><svg viewBox="0 0 20.6 26.24"><path d="M10.3,0A10.28,10.28,0,0,0,0,10.26s0,0,0,0a11.63,11.63,0,0,0,.85,4.15A35.44,35.44,0,0,0,10.3,26.24a35.19,35.19,0,0,0,9.45-11.87,11.63,11.63,0,0,0,.85-4.15A10.26,10.26,0,0,0,10.3,0h0Zm0,18.18a8,8,0,1,1,8-8h0A8,8,0,0,1,10.3,18.18Z" transform="translate(0 0)"/></svg><div class="label">' + val.label + '<div class="arrow"></div></div>'
					});

					if ( ctrl.active.index === index ){
						marker = L.marker(val.coords, {
							icon: activeIcon
						}).addTo(map).on('click', function(){
							ctrl.change({
								itinerary: ctrl.slug, 
								stop: val.slug, 
								addClass: undefined, 
								removeClass: undefined, 
								index: index
							});
						});

						activeIndex = index;
					} else {
						marker = L.marker(val.coords, {
							icon: defaultIcon
						}).addTo(map).addTo(map).on('click', function(){
							ctrl.change({
								itinerary: ctrl.slug, 
								stop: val.slug, 
								addClass: undefined, 
								removeClass: undefined, 
								index: index
							});
						});
					}

					if ( ctrl.active.index === ctrl.points.length ){
						activeIndex = ctrl.points.length - 1;
					}

					markers.push(marker);
				});

				scope.$watch('vm.active.index', function(){
					if ( ctrl.points[ctrl.active.index] && ctrl.active.index < ctrl.points.length ){
						var defaultIcon = L.divIcon({
							className: 'itinerary-map-marker',
							html: '<div class="index">' + Math.abs(activeIndex + 1) + '</div><svg viewBox="0 0 20.6 26.24"><path d="M10.3,0A10.28,10.28,0,0,0,0,10.26s0,0,0,0a11.63,11.63,0,0,0,.85,4.15A35.44,35.44,0,0,0,10.3,26.24a35.19,35.19,0,0,0,9.45-11.87,11.63,11.63,0,0,0,.85-4.15A10.26,10.26,0,0,0,10.3,0h0Zm0,18.18a8,8,0,1,1,8-8h0A8,8,0,0,1,10.3,18.18Z" transform="translate(0 0)"/></svg><div class="label">' + ctrl.points[activeIndex].label + '<div class="arrow"></div></div>'
						});

						var activeIcon = L.divIcon({
							className: 'itinerary-map-marker-active',
							html: '<div class="index">' + Math.abs(ctrl.active.index + 1) + '</div><svg viewBox="0 0 20.6 26.24"><path d="M10.3,0A10.28,10.28,0,0,0,0,10.26s0,0,0,0a11.63,11.63,0,0,0,.85,4.15A35.44,35.44,0,0,0,10.3,26.24a35.19,35.19,0,0,0,9.45-11.87,11.63,11.63,0,0,0,.85-4.15A10.26,10.26,0,0,0,10.3,0h0Zm0,18.18a8,8,0,1,1,8-8h0A8,8,0,0,1,10.3,18.18Z" transform="translate(0 0)"/></svg><div class="label">' + ctrl.points[ctrl.active.index].label + '<div class="arrow"></div></div>'
						});

						markers[activeIndex].setIcon(defaultIcon);
						markers[ctrl.active.index].setIcon(activeIcon);
						map.panTo(ctrl.points[ctrl.active.index].coords);
						activeIndex = ctrl.active.index;
					}
				});

				scope.$on('Viewport:Update', function(){
					angular.element(el).attr('style', '');
					//updateSize();
					map.invalidateSize();
				});

				scope.$on("$destroy", function() {
					map.remove();
				});

				function updateSize(){
					width = el.prop('offsetWidth');
					height = el.prop('offsetHeight');
					angular.element(el).attr('style', 'height: ' + height + 'px; width: ' + width + 'px;');
					return;
				}
			}
		}
	}

	Controller.$inject = ['globals'];

	function Controller(globals) {
		var vm = this;
	}

})();
