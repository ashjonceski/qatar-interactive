(function() {
	/* jshint validthis: true */
	'use strict';

	angular.module('qatarApp.itinerary.socialShare', [])
		.directive('socialShare', socialShare);

	socialShare.$inject = [];

	function socialShare() {
		var directive = {
			restrict: 'EA',
			templateUrl: 'app.itinerary/social-share/social-share.template.html',
			scope: {
				itineraryItem: '='
			},
			controller: Controller,
			controllerAs: 'vm',
			bindToController: true,
			link: linkFunc,
			replace: true
		};

		return directive;

		function linkFunc(scope, el, attr, ctrl) {

		}
	}

	Controller.$inject = ['globals'];

	function Controller(globals) {
		var vm = this;

		vm.shareOpen = false;

		vm.openShareWindow = openShareWindow;
		vm.toggleShare = toggleShare;

		function toggleShare(){
			vm.shareOpen = vm.shareOpen ? false : true;
			return;
		}

		function openShareWindow(network) {
			var twitterBaseUrl = "https://twitter.com/home?status=";
			var facebookBaseUrl = "https://www.facebook.com/dialog/share?app_id=180444840287&display=popup&href=";
			var pinterestBaseUrl = "http://www.pinterest.com/pin/create/button/?url=";
			var googleplusBaseUrl = "https://plus.google.com/share?url=";
			var sharemessage = vm.itineraryItem[network];
			var pic = '@@assetPath@@/desktop/' + vm.itineraryItem.shareimage;
			var shareWindow = '';
			var guardianUrl = window.location.href;

			if (network === "twitter") {
				shareWindow =
					twitterBaseUrl +
					encodeURIComponent(sharemessage) +
					"%20" +
					encodeURIComponent(guardianUrl);
			} else if (network === "facebook") {
				shareWindow =
					facebookBaseUrl +
					encodeURIComponent(guardianUrl) +
					"&redirect_uri=http://www.theguardian.com";
			} else if (network === "pinterest") {
				shareWindow =
					pinterestBaseUrl +
					encodeURIComponent(guardianUrl) +
					"&media=" +
					encodeURIComponent(pic) +
					"&description=" +
					encodeURIComponent(sharemessage);
			} else if ( network === "googleplus"){
				shareWindow = 
					googleplusBaseUrl +
					encodeURIComponent(guardianUrl);
			}

			window.open(shareWindow, network + "share", "width=640,height=320");

			// track share
			ga('glabsau.send', 'social', network, 'share', guardianUrl);

			return;
		}
	}

})();
