(function() {
	/* jshint validthis: true */
	'use strict';

	angular.module('qatarApp.itinerary.dotNav', [])
		.directive('dotNav', dotNav);

	dotNav.$inject = [];

	function dotNav() {
		var directive = {
			restrict: 'EA',
			templateUrl: 'app.itinerary/dot-nav/dot-nav.template.html',
			scope: {
				itinerary: '=',
				active: '=',
				itineraryItems: '=',
				change: '&'
			},
			controller: Controller,
			controllerAs: 'vm',
			bindToController: true,
			link: linkFunc,
			replace: true
		};

		return directive;

		function linkFunc(scope, el, attr, ctrl) {

		}
	}

	Controller.$inject = ['globals'];

	function Controller(globals) {
		var vm = this;
	}

})();
