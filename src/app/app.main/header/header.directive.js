(function() {
	/* jshint validthis: true */
	'use strict';

	angular.module('qatarApp.main.header', [])
		.directive('mainHeader', header);

	header.$inject = [];

	function header() {
		var directive = {
			restrict: 'EA',
			templateUrl: 'app.main/header/header.template.html',
			scope: {},
			controller: Controller,
			controllerAs: 'vm',
			bindToController: true,
			link: linkFunc,
			replace: true
		};

		return directive;

		function linkFunc(scope, el, attr, ctrl) {

		}
	}

	Controller.$inject = [];

	function Controller() {
		var vm = this;
	}

})();
