(function() {
	/* jshint validthis: true */

	'use strict';

	angular
		.module('qatarApp.main', [
			'qatarApp.main.panel',
			'qatarApp.main.panelEmblem',
			'qatarApp.main.header'
		])
		.controller('mainCtrl', Controller);

	Controller.$inject = ['$state', '$timeout', '$scope', 'globals', 'services'];

	function Controller($state, $timeout, $scope, globals, services) {
		var vm = this;

		vm.pageClass = globals.showVideo ? 'page-main video-playing' : 'page-main';
		vm.routeId = $state.current;
		vm.itineraries = globals.data.itineraries;
		vm.showVideo = globals.showVideo;

		activate();

		function activate() {
			$scope.$watch('vm.showVideo', function(){
				vm.pageClass = globals.showVideo ? 'page-main video-playing' : 'page-main';
				vm.showVideo = globals.showVideo;
			});
		}
	}

})();