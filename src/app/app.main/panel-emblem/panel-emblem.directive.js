(function() {
	/* jshint validthis: true */
	'use strict';

	angular.module('qatarApp.main.panelEmblem', [])
		.directive('panelEmblem', panelEmblem);

	panelEmblem.$inject = [];

	function panelEmblem() {
		var directive = {
			restrict: 'EA',
			templateUrl: 'app.main/panel-emblem/panel-emblem.template.html',
			scope: {
				icon: '='
			},
			controller: Controller,
			controllerAs: 'vm',
			bindToController: true,
			link: linkFunc,
			replace: true
		};

		return directive;

		function linkFunc(scope, el, attr, ctrl) {

		}
	}

	Controller.$inject = [];

	function Controller() {
		var vm = this;
	}

})();
