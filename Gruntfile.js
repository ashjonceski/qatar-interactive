'use strict';
var pkg = require('./package.json');
var currentTime = +new Date();
var versionedAssetPath = 'assets';
var CDN = 'https://interactive.guim.co.uk/';
var deployAssetPath = CDN + pkg.config.s3_folder + versionedAssetPath;
var localAssetPath = '/assets';

module.exports = function(grunt) {
  var isDev = !(grunt.cli.tasks && grunt.cli.tasks[0] === 'deploy');
  grunt.initConfig({

    connect: {
      server: {
        options: {
          port: pkg.config.port,
          hostname: 'localhost',
          livereload: 35729,
          open: true,
          base: './build/',
          middleware: function (connect, options, middlewares) {
            // inject a custom middleware http://stackoverflow.com/a/24508523 
            middlewares.unshift(function (req, res, next) {
                res.setHeader('Access-Control-Allow-Origin', '*');
                res.setHeader('Access-Control-Allow-Methods', '*');
                return next();
            });
            return middlewares;
          }
        }
      }
    },

    bowerRequirejs: {
        all: {
            rjsConfig: './src/js/require.js',
            options: {
                baseUrl: './src/js/'
            }
        }
    },

    sass: {
      options: {
            sourcemap: 'inline'
       },
       build: {
         files: { 'build/assets/css/main.css': ['src/css/main.scss', 'src/app/*.scss', 'src/app/**/*.scss', 'src/css/libs/leaflet.css'] }
       }
    },

    autoprefixer: {
        options: {
            map: true
        },
        css: { src: 'build/assets/css/*.css' }
    },

    clean: ['build/', '.tmp'],

    jshint: {
      options: {
          jshintrc: true,
          force: true 
      },
	  files: [
	    'Gruntfile.js',
	    'src/**/*.js',
	    '!src/js/require.js',
	    '!src/js/libs/preloadjs-0.6.2.min.js',
	    '!src/js/libs/leaflet.js',
	    '!src/js/libs/mapbox.js'
	  ]
    },

    requirejs: {
      compile: {
        options: {
          baseUrl: './src/js/',
          mainConfigFile: './src/js/require.js',
          optimize: (isDev) ? 'none' : 'uglify2',
          inlineText: true,
          name: 'almond',
          out: 'build/assets/js/main.js',
          generateSourceMaps: true,
          preserveLicenseComments: false,
          useSourceUrl: true,
          include: ['main'],
          wrap: {
            start: 'define(["require"],function(require){var req=(function(){',
            end: 'return require; }()); return req; });'
          }

        }
      }
    },

    watch: {
	  options: {
		livereload: 35729,
		spawn: false
	  },
      scripts: {
        files: [
          'src/**/*.js',
          'src/**/*.json',
          'src/js/templates/*.html'
        ],
        tasks: ['jshint', 'requirejs', 'replace:local']
      },
      html: {
        files: ['src/*.html', 'src/embed/*.html'],
        tasks: ['copy', 'replace:local']
      },
      css: {
        files: ['src/css/**/*.*'],
        tasks: ['sass', 'autoprefixer', 'replace:local', 'cssmin']
      },
      imgs: {
        files: [
        	'src/imgs/*.*', 
        	'src/imgs/**/*.*',
        	'src/mobile/imgs/*.*', 
        	'src/mobile/imgs/**/*.*',
        	'src/desktop/imgs/*.*', 
        	'src/desktop/imgs/**/*.*'
        ],
        tasks: ['copy']
      },
      vids: {
        files: [
        	'src/vids/*.*', 
        	'src/vids/**/*.*',
        	'src/mobile/vids/*.*', 
        	'src/mobile/vids/**/*.*',
        	'src/desktop/vids/*.*', 
        	'src/desktop/vids/**/*.*'
        ],
        tasks: ['copy']
      },
      app: {
      	files: ['src/app/*.*', 'src/app/**/*.*'],
        tasks: ['sass', 'jshint', 'ngtemplates', 'concat', 'replace:local', 'cssmin']
      }
    },

    copy: {
      build: {
        files: [
          {
              cwd: 'src/',
              src: ['index.html', 'boot.js', 'embed/*.*'],
              expand: true,
              dest: 'build/'
          },
          //{
          //    cwd: 'src/',
         //     src: [
         //     	'imgs/*.*', 
	     //   	'imgs/**/*.*',
	      //  	'mobile/imgs/*.*', 
	       // 	'mobile/imgs/**/*.*',
	      //  	'desktop/imgs/*.*', 
	      //  	'desktop/imgs/**/*.*'
         //     ],
          //    expand: true,
         //     dest: 'build/assets/'
          //},
          {
              cwd: 'src/',
              src: [
              	'vids/*.*', 
	        	'vids/**/*.*',
	        	'mobile/vids/*.*', 
	        	'mobile/vids/**/*.*',
	        	'desktop/vids/*.*', 
	        	'desktop/vids/**/*.*'
              ],
              expand: true,
              dest: 'build/assets/'
          },
          {
              cwd: 'src/',
              src: ['imgs/libs/*.*'],
              expand: true,
              dest: 'build/assets/'
          },
          {
              src: 'bower_components/curl/dist/curl/curl.js',
              dest: 'build/assets/js/curl.js'
          }
        ]
      }
    },

    concat: {
		vendor: {
			src: [
				'bower_components/lodash/dist/lodash.min.js',
				'bower_components/hammerjs/hammer.min.js',
				'src/js/libs/preloadjs-0.6.2.min.js',
				'src/js/libs/leaflet.js',
				'bower_components/angular/angular.min.js',
				'bower_components/angular-animate/angular-animate.min.js',
				'bower_components/angular-ui-router/release/angular-ui-router.min.js',
				'src/app/app.module.js',
				'src/app/app.routes.js',
				'src/app/app.globals.js',
				'src/app/app.filters.js',
				'src/app/app.services.js',
				'src/app/app.controller.js',
				'src/app/**/*.js',
				'.tmp/js/app.templates.js'
			],
			dest: 'build/assets/js/app.js'
		},
		styling: {
			src: [
				'bower_components/normalize-css/normalize.css',
				'src/css/libs/leaflet.css',
				'bower_components/perfect-scrollbar/css/perfect-scrollbar.min.css',
				'build/assets/css/main.css'
			],
			dest: 'build/assets/css/main.css'
		}
    },

    imagemin: {
      options: {
        optimizationLevel: 3,
        svgoPlugins: [{ removeViewBox: false }],
      },
      dynamic: {
        files: [{
          expand: true,
          cwd: 'src/',
          src: [
          	'imgs/**/*.{png,jpg,gif,svg}',
          	'desktop/imgs/**/*.{png,jpg,gif,svg}',
          	'mobile/imgs/**/*.{png,jpg,gif,svg}'
          ],
          dest: 'build/assets/'
        }]
      }
    },

    ngtemplates:  {
	  qatarApp: {
	    cwd:      'src/app',
	    src:      '**/*.html',
	    dest:     '.tmp/js/app.templates.js'
	  }
	},

    replace: {
        prod: {
            options: {
                patterns: [{
                  match: /@@assetPath@@/g,
                  replacement: deployAssetPath 
                },
                {
                  match: /images\//g,
                  replacement: deployAssetPath
                }]
            },
            files: [{
                src: ['build/**/*.html', 'build/**/*.js', 'build/**/*.css'],
                dest: './'
            }]
        },
        local: {
            options: {
                patterns: [{
                  match: /@@assetPath@@/g,
                  replacement: localAssetPath
                },
                {
                  match: /images\//g,
                  replacement: localAssetPath + '/img/'
                }]
            },
            files: [{
                src: ['build/**/*.html', 'build/**/*.js', 'build/**/*.css'],
                dest: './'
            }]
        }
    },

    s3: {
        options: {
            access: 'public-read',
            bucket: 'gdn-cdn',
            maxOperations: 20,
            dryRun: (grunt.option('test')) ? true : false,
            headers: {
                CacheControl: 180,
            },
            gzip: true,
            gzipExclude: ['.jpg', '.gif', '.jpeg', '.png']
        },
        base: {
            files: [{
                cwd: 'build',
                src: ['*.*', 'embed/*.*'],
                dest: pkg.config.s3_folder
            }]
        },
        assets: {
            options: {
                headers: {
                    CacheControl: 3600,
                }
            },
            files: [{
                cwd: 'build',
                src: versionedAssetPath + '/**/*.*',
                dest: pkg.config.s3_folder
            }]
        }
    },

    rename: {
        main: {
            files: [
                {
                    src: 'build/assets',
                    dest: 'build/' + versionedAssetPath
                }
            ]
        }
    },

    // gzip assets 1-to-1 for production
	compress: {
		main: {
			options: {
				mode: 'gzip'
			},
			expand: true,
			cwd: 'build/',
			src: ['**/*'],
			dest: 'build/'
		}
	},

	uglify: {
		options: {
			mangle: false
		},
		js: {
			files: [{
				expand: true,
				cwd: 'build/assets/js/',
				src: '**/*.js',
				dest: 'build/assets/js/'
			}]
		}
	},

	cssmin: {
	  target: {
	    files: [{
	      expand: true,
	      cwd: 'build/assets/css/',
	      src: ['*.css', '!*.min.css'],
	      dest: 'build/assets/css/',
	      ext: '.min.css'
	    }]
	  }
	}

  });

  // Task pluginsk
  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-contrib-connect');
  grunt.loadNpmTasks('grunt-contrib-requirejs');
  grunt.loadNpmTasks('grunt-contrib-sass');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-contrib-imagemin');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-aws');
  grunt.loadNpmTasks('grunt-autoprefixer');
  grunt.loadNpmTasks('grunt-replace');
  grunt.loadNpmTasks('grunt-contrib-rename');
  grunt.loadNpmTasks('grunt-bower-requirejs');
  grunt.loadNpmTasks('grunt-newer');
  grunt.loadNpmTasks('grunt-angular-templates');
  grunt.loadNpmTasks('grunt-contrib-compress');
  grunt.loadNpmTasks('grunt-contrib-uglify');

  // Tasks
  grunt.registerTask('build', [
    'jshint',
    'clean',
    'sass',
    'autoprefixer',
    'bowerRequirejs',
    'requirejs',
    'copy:build',
    /*'newer:imagemin',*/
    'imagemin',
    'ngtemplates',
    'concat',
    'cssmin'
  ]);
  
  grunt.registerTask('default', [
      'build',
      'replace:local',
      'connect',
      'watch'
  ]);

  grunt.registerTask('prod', [
      'build',
      'replace:prod',
      'uglify',
      'compress'
  ]);
  
  grunt.registerTask('deploy', [
      'build',
      'rename',
      'replace:prod',
      's3'
  ]);
};
